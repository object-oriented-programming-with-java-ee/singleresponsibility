package employee;

public class PayCalculator {

	public Double calculatePay(Integer employeeId)
	{
		// this method is where we calculate the pay of an employee
		// it will probably call several private implementation methods 
	}
	
	private void someImplementationDetail()
	{
		// this method is private and not exposed in the facade
		// we can abstract implementation into this class and hide it behind
		// the facade
	}
	
}
