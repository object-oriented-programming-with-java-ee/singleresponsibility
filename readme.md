# Single Responsibility Principal

The SRP states that a module should be responsible to one, and only one, actor.

In this project we have a CFO who is interested in the total cost of an employee's wages and we have a COO who is interested in seeing how employees are spending their time.

There are therefore two actors who are interested in "Employee".

We separate the functionality for each actor by placing it into its own class.

The information for the CTO is handled in the "PayCalculator" class and the COO reports are generated in "HourReporter".

In order to make it more convenient for ourselves we create a "Facade" that gives us easy access to the underlying objects.

If the CTO asks us to make a change to the way we calculate wages then this will have no impact on the COO reports, and vice versa.

Note that this is just one way of implementing the SRP, and the Facade is just one way of making it more convenient.