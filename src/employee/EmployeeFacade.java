package employee;

public class EmployeeFacade {

	private HourReporter hourReporter;
	
	private PayCalculator payCalculator;
	
	public EmployeeFacade() {
		hourReporter = new HourReporter();
		payCalculator = new PayCalculator();
	}
	
	public Double calculatePay(Integer employeeId)
	{
		return payCalculator.calculatePay(employeeId);
	}
	
	public String reportHours(Integer employeeId)
	{
		return hourReporter.reportHours(employeeId);
	}
	
}
